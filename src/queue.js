const Queue = require('bull')
const {Redis: {createClient}} = require('alcumus-data-source-server')
const events = require('./events')

const subscriber = createClient()
const client = createClient()

const standardOptions = {
    createClient(type) {
        switch(type) {
            case 'client':
                return client
            case 'subscriber':
                return subscriber
            default:
                return createClient()
        }
    }
}

const queues = {}

function createQueue(name, options) {
    let queue = {Queue, options}
    events.emit(`create.queue.${name}`, queue)
    return new queue.Queue(name, {...standardOptions, ...queue.options})
}

module.exports = function queue(name, opts) {
    return queues[name] = queues[name] || createQueue(name, opts)
}
