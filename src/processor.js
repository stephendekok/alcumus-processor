const events = require('./events');
const isObject = require('lodash/isObject');
const queue = require('./queue')
const {listen} = require('./listener')


module.exports = function processor(type, fn) {
    listen(type)
    const handler = async (event, ...params) => {
        params[1].hasRun = true;
        let result;
        if(fn.length >= 2) {
            result = await fn.call(event, ...params);
        } else {
            result = await fn.call(params[0].payload, {input: params[1].documents[0]});
        }
        if(isObject(result) && params.length > 1) {
            params[1].result = Object.assign({}, params[1].result, result);
        }
    };
    events.on(`process.${type}`, handler);
    return () => {
        events.off(`process.${type}`, handler);
    };
};

module.exports.clear = function (type) {
    events.removeAllListeners(type ? `process.${type}` : undefined);
};
