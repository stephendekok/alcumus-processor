/* globals describe, beforeEach, it */

global.regeneratorRuntime = require('regenerator-runtime/runtime');
require('dotenv').config();
const chai = require('chai');
chai.use(require('chai-as-promised'));
const { expect } = chai;
const { Database: { query }, MutationRelay } = require('alcumus-data-source-server');
MutationRelay.enable();

function delay (timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout);
    });
}

const { get, set, list, create, createLikeness, initialize, initializeWithBehaviours } = require('./data');
describe('Data functions', function () {
    beforeEach(async function () {
        //language=MySQL
        await query('DROP DATABASE IF EXISTS `database`;');
        //language=MySQL
        await query('CREATE DATABASE IF NOT EXISTS `database`;');
        await delay(20);
        await query(`CREATE TABLE IF NOT EXISTS \`database\`.\`test\`
                     (
                         \`row_id\`   int(11)     NOT NULL AUTO_INCREMENT,
                         \`_id\`      varchar(50) NOT NULL,
                         \`data\`     json             DEFAULT NULL,
                         \`created\`  timestamp   NULL DEFAULT CURRENT_TIMESTAMP,
                         \`modified\` timestamp   NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                         \`state\`    varchar(255) GENERATED ALWAYS AS (json_unquote(
                                 json_extract(\`data\`, _utf8mb4 '$.state'))) VIRTUAL,
                         PRIMARY KEY (\`row_id\`),
                         UNIQUE KEY \`_id\` (\`_id\`),
                         KEY \`state\` (\`state\`)
                     ) ENGINE = InnoDB
                       AUTO_INCREMENT = 7
                       DEFAULT CHARSET = utf8mb4
                       COLLATE = utf8mb4_0900_ai_ci;`);
        await query(`INSERT INTO \`database\`.\`test\` (\`row_id\`, \`_id\`, \`data\`,
                                                        \`created\`, \`modified\`)
                     VALUES (1, '_fNrCwayqF:database/test',
                             '{\\"_id\\": \\"_fNrCwayqF:database/test\\", \\"hello\\": 1}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (2, 'tve1iw-8SQ:database/test',
                             '{\\"_id\\": \\"tve1iw-8SQ:database/test\\", \\"hello\\": 2}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (3, 'nAXOXDJKCM:database/test',
                             '{\\"_id\\": \\"nAXOXDJKCM:database/test\\", \\"hello\\": 3}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (4, 'a8xNdVzPuP6:database/test',
                             '{\\"_id\\": \\"a8xNdVzPuP6:database/test\\", \\"hello\\": 4}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (5, 'ivLs7lIK8tr:database/test',
                             '{\\"_id\\": \\"ivLs7lIK8tr:database/test\\", \\"hello\\": 5}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (6, '9k-mEN2SPj8:database/test',
                             '{\\"_id\\": \\"9k-mEN2SPj8:database/test\\", \\"hello\\": 6}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39')
                     ON DUPLICATE KEY UPDATE data = values(data);`);
    });
    it('will provide a list of items', async function () {
        let result = await list('database', 'test');
        expect(result.length).to.eq(6);
    });
    it('will error if getting an entry without a valid id', async function () {
        await expect(get()).to.be.rejectedWith('Must be supplied an id');
        await expect(get('a')).to.be.rejectedWith('id must include a');
        await expect(get('a:b')).to.be.rejectedWith('id must include a');
        await expect(get('a:b/')).to.be.rejectedWith('id must include a');
    });
    it('will error if creating an entry without an id', async function () {
        await expect(create()).to.be.rejectedWith('You must supply a data object');
        await expect(create({ a: 1 })).to.be.rejectedWith('You must supply an id');
        await expect(create({
            a: 1,
            _id: 'a'
        })).to.be.rejectedWith('You must supply a type');
        await expect(create({ a: 1, _id: 'a:b' })).to.be.rejectedWith('Type must be');
        await expect(create({ a: 1, _id: 'a:b/' })).to.be.rejectedWith('Type must be');
        await expect(create({
            a: 1,
            _id: 'a:b/c',
            _original: {}
        })).to.be.rejectedWith('Object has already been created');
    });
    it('should be able to get an entry', async function () {
        let row = await get('tve1iw-8SQ:database/test');
        expect(row.hello).to.eq(2);
        expect(row._original).to.exist;
    });
    it('should be able to set some value', async function () {
        let row = await get('tve1iw-8SQ:database/test');
        row.hello = 121;
        await set(row);
        //If we got here then it fully tracked the item
        for (let i = 0; i < 10; i++) {
            row.hello = i + 200;
            await set(row);
        }
    });
    it('should be able to create a new record', async function () {
        await create({ _id: 'banana:database/test', a: 'hello' });
        let [rows] = await query('SELECT * FROM `database`.test WHERE _id=\'banana:database/test\'');
        expect(rows.length).to.eq(1);
    });
    it('should be able to create a likeness', function () {
        let record = { _id: 'hello:database/test', hello: 1, _original: {} };
        let copy = createLikeness(record);
        expect(copy._id).to.match(/:database\/test/);
        expect(copy.hello).to.eq(1);
        expect(copy._id).not.to.eq(record._id);
        expect(copy._original).to.be.undefined;
    });
    it('should initialize a new object with a database type and new id', async function () {
        let item = initialize('database', 'test');
        expect(item._id).to.match(/:database\/test/);
    });
    it('should initialize an item with behaviours', async function () {
        let item = initializeWithBehaviours('database', 'test');
        expect(item._id).to.match(/:database\/test/);
        expect(item.behaviours).to.exist;
        expect(item.behaviours.state).to.eq('default');
    });
    it('should have the properties of the target item when initializing', async function () {
        let item = initialize('database', 'test', { a: 1 });
        expect(item._id).to.match(/:database\/test/);
        expect(item.a).to.eq(1);
    });
});
