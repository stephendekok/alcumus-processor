const queue = require('./queue')
const events = require('./events')
const { using } = require('./locks')
const uniq = require('lodash/uniq')
const cls = require('alcumus-cls')
const { publish } = require('alcumus-pub-sub')
const { MutationRelay, broadcast } = require('alcumus-data-source-server')


function listen(type) {
    queue(`process.${type}`).process(async function({ data: { process, metadata }, id: jobId }) {
        try {
            process.documents = process.documents || []
            process.references = process.references || []
            const result = {
                ok: true,
                processed: [],
                notAuthorised: {},
                errors: [],
                client: { events: [], notifications: [] },
            }
            cls.process = process
            cls._holdContext = true

            await using(process.documents.map(document => `process.${document}`), async (extend) => {
                try {
                    let documents = (await Promise.all(process.documents.map(document => get(document)))).filter(d => !!d)
                    let references = (await Promise.all(process.references.map(document => get(document)))).filter(d => !!d)
                    documents.forEach(document => {
                        document.hash = () => hash(document)
                        documents[document._id] = document
                    })
                    references.forEach(document => {
                        references[document._id] = document
                    })
                    let parameters = {
                        ...metadata, documents, references, extend, result: result.client, hasRun: false,
                        emit(event, ...params) {
                            result.client.events.push({ event, params })
                        },
                        notify(message, title, caption) {
                            result.client.notifications.push({ message, title, caption })
                        },
                    }

                    cls.documents = documents
                    cls.references = references
                    cls.result = result.client
                    cls.extend = extend
                    cls.emit = parameters.emit
                    cls.notify = parameters.notify
                    cls.jobId = jobId

                    await events.emitAsync(`queue.prepare.execute.${process.type}`, parameters)
                    await events.emitAsync(`prepare.${process.type}`, parameters)
                    await events.emitAsync(`process.${process.type}`, process, parameters)
                    await Promise.all(documents.map(document => set(document)))
                    result.errors.push(!parameters.hasRun ? 'Nothing to do' : false)
                } catch (e) {
                    const error = e || { message: 'Unknown Error' }
                    result.errors.push(error.message)
                }
                let dbs = uniq(process.documents.map(doc => doc.split(':')[1].split('/')[0]))
                if (process.$id) {
                    MutationRelay.flush()
                    dbs.forEach(db => {
                        setTimeout(() => broadcast('complete', process.$id, db))
                    })
                }
                let packet = { message: { jobId, id: process.$id, result, type }, client: metadata.clientId }
                await events.emitAsync(`jobDone.${jobId}`, packet)
                if (metadata.clientId) {
                    broadcast(`jobDone.${jobId}`, packet, metadata.clientId, 0.001)
                } else {
                    await publish('job', JSON.stringify(packet))
                }
            })
        } catch (err) {
            console.error(err)
        }
    })
}

module.exports = { listen }
