const events = require('./events')
const { using } = require('./locks')
const { get, set } = require('./data')
const { hash } = require('alcumus-app-datasource-mutations/dist/hash')
const {
    DataSource: { initialize },
    broadcast,
    MutationRelay,
} = require('alcumus-data-source-server')
const uniq = require('lodash/uniq')
const queue = require('./queue')
const { generate } = require('shortid')
const { subscribe } = require('alcumus-pub-sub')

setTimeout(function() {
    subscribe('job', function(channel, message) {
        let packet = JSON.parse(message)
        events.emit(`jobFinished.${packet.message.jobId}`, packet.message.result)
    })
})

async function run(processes, req, download) {
    let result = {
        ok: true,
        processed: [],
        notAuthorised: {},
        errors: [],
        client: { events: [], notifications: [] },
    }
    processes = Array.isArray(processes) ? processes : [processes]
    while (processes.length) {
        let process = processes.shift()
        let params = { process, canRun: true, req, reason: [], enqueue: false }
        await events.emitAsync(`can-run.${process.type}`, params)
        if (!params.canRun) {
            result.notAuthorised[process.type] = params.reason.length
                ? params.reason
                : 'unauthorised'
            result.errors.push(result.notAuthorised[process.type])
            result.processed.push(false)
            continue
        }
        await events.emitAsync(`should-enqueue.${process.type}`, params)

        process.documents = process.documents || []
        process.references = process.references || []
        await using(
            process.documents.map(document => `process.${document}`),
            async extend => {
                try {
                    result.processed.push(true)
                    let documents = await Promise.all(
                        process.documents.map(document => get(document)),
                    )
                    let references = await Promise.all(
                        process.references.map(document => get(document)),
                    )
                    documents.forEach(document => {
                        document.hash = () => hash(document)
                        documents[document._id] = document
                    })
                    references.forEach(document => {
                        references[document._id] = document
                    })
                    let parameters = {
                        req,
                        documents,
                        references,
                        extend,
                        result: result.client,
                        hasRun: false,
                        emit(event, ...params) {
                            result.client.events.push({ event, params })
                        },
                        notify(message, title, caption) {
                            result.client.notifications.push({ message, title, caption })
                        },
                        download(...params) {
                            download(...params)
                        },
                    }
                    await events.emitAsync(`prepare.${process.type}`, parameters)
                    await events.emitAsync(
                        `process.${process.type}`,
                        process,
                        parameters,
                    )
                    await Promise.all(documents.map(document => set(document)))
                    result.errors.push(!parameters.hasRun ? 'Nothing to do' : false)
                } catch (e) {
                    const error = e || { message: 'Unknown Error' }
                    result.errors.push(error.message)
                    throw error
                }
            },
        )
        let dbs = uniq(
            process.documents.map(doc => doc.split(':')[1].split('/')[0]),
        )
        if (process.$id) {
            MutationRelay.flush()
            dbs.forEach(db => {
                setTimeout(() => broadcast('complete', process.$id, db), 100)
            })
        }
    }

    return result
}

async function handleEnqueue(req, res) {
    try {
        let {
            body: process,
            query: { client: clientId },
        } = req
        process.$id = process.$id || generate()
        let params = { process, canRun: true, req, reason: [] }
        await events.emitAsync(`can-run.${process.type}`, params)
        if (!params.canRun) {
            res.statusMessage = 'not authorised'
            res.status(500).send('cannot run')
        }
        let metadata = { clientId }
        await events.emitAsync(
            `queue.prepare.enqueue.${process.type}`,
            metadata,
            req,
        )
        const job = await queue(`process.${process.type}`).add(
            { process, metadata },
            { jobId: process.$id },
        )
        res.status(200).send({ id: job.id })
    } catch (e) {
        res.statusMessage = e.message
        res.status(500).send(e.stack)
    }
}

async function process(process, metadata = {}) {
    process.$id = process.$id || generate()
    await events.emitAsync(`queue.prepare.enqueue.${process.type}`, metadata)
    const job = await queue(`process.${process.type}`).add(
        { process, metadata },
        { jobId: process.$id },
    )
    return new Promise(resolve => events.once(`jobFinished.${job.id}`, resolve))
}

async function handleProcess(req, res) {
    try {
        let { body } = req
        let downloaded = false
        let result = await run(body, req, (...params) => {
            downloaded = true
            res.download(...params)
            res.end()
        })
        if (!downloaded) {
            res.status(200).send(result || { ok: true })
        }
    } catch (e) {
        res.statusMessage = e.message
        res.status(500).send(e.stack)
    }
}

module.exports = {
    initialize(app) {
        initialize(app)
        app.post('/process', handleProcess)
        app.post('/enqueue', handleEnqueue)
    },
}

module.exports.run = run
module.exports.process = process
