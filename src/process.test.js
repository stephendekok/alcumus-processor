/* globals describe, beforeEach, it */

global.regeneratorRuntime = require('regenerator-runtime/runtime');
require('dotenv').config();
const chai = require('chai');
const sinon = require('sinon');
chai.use(require('chai-as-promised'));
const {expect} = chai;
const {Database: {query}, MutationRelay} = require('alcumus-data-source-server');
MutationRelay.enable();

function delay(timeout) {
    return new Promise(function (resolve) {
        setTimeout(resolve, timeout);
    });
}

const processor = require('./processor');
const app = require('../app');
const request = require('supertest');
const events = require('./events');


function awaitEvent(name) {
    return new Promise(resolve=>{
        events.once(name, resolve)
    })
}

describe('Processor', function () {
    beforeEach(async function () {
        //language=MySQL
        await query('DROP DATABASE IF EXISTS `database`;');
        //language=MySQL
        await query('CREATE DATABASE IF NOT EXISTS `database`;');
        await delay(20);
        await query(`CREATE TABLE IF NOT EXISTS \`database\`.\`test\`
                     (
                       \`row_id\`   int(11)     NOT NULL AUTO_INCREMENT,
                       \`_id\`      varchar(50) NOT NULL,
                       \`data\`     json        NULL DEFAULT NULL,
                       \`created\`  timestamp   NULL DEFAULT CURRENT_TIMESTAMP,
                       \`modified\` timestamp   NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                       \`state\`    varchar(255) GENERATED ALWAYS AS (json_unquote(
                           json_extract(\`data\`, _utf8mb4 '$.state'))) VIRTUAL,
                       PRIMARY KEY (\`row_id\`),
                       UNIQUE KEY \`_id\` (\`_id\`),
                       KEY \`state\` (\`state\`)
                     ) ENGINE = InnoDB
                       AUTO_INCREMENT = 7
                       DEFAULT CHARSET = utf8mb4
                       COLLATE = utf8mb4_0900_ai_ci;`);
        await query(`INSERT INTO \`database\`.\`test\` (\`row_id\`, \`_id\`, \`data\`,
                                                        \`created\`, \`modified\`)
                     VALUES (1, '_fNrCwayqF:database/test',
                             '{\\"_id\\": \\"_fNrCwayqF:database/test\\", \\"hello\\": 1}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (2, 'tve1iw-8SQ:database/test',
                             '{\\"_id\\": \\"tve1iw-8SQ:database/test\\", \\"hello\\": 2}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (3, 'nAXOXDJKCM:database/test',
                             '{\\"_id\\": \\"nAXOXDJKCM:database/test\\", \\"hello\\": 3}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (4, 'a8xNdVzPuP6:database/test',
                             '{\\"_id\\": \\"a8xNdVzPuP6:database/test\\", \\"hello\\": 4}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (5, 'ivLs7lIK8tr:database/test',
                             '{\\"_id\\": \\"ivLs7lIK8tr:database/test\\", \\"hello\\": 5}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39'),
                            (6, '9k-mEN2SPj8:database/test',
                             '{\\"_id\\": \\"9k-mEN2SPj8:database/test\\", \\"hello\\": 6}',
                             '2018-12-25 23:08:39', '2018-12-25 23:08:39')
                     ON DUPLICATE KEY UPDATE data = values(data);`);
    });
    beforeEach(() => processor.clear());
    it('should be able to call the process function', async function () {
        await request(app)
            .post('/process')
            .send([{type: 'test'}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
    });
    it('should call a processor of a suitable type', async function () {
        let fn = sinon.stub();
        processor('test', fn);
        await request(app)
            .post('/process')
            .send([{type: 'test'}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        expect(fn.calledOnce).to.be.true;
    });
    it('should support objects as well as arrays', async function () {
        let fn = sinon.stub();
        processor('test', fn);
        await request(app)
            .post('/process')
            .send({type: 'test'})
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        expect(fn.calledOnce).to.be.true;
    });
    it.only('should support enqueuing objects', async function () {
        let fn = sinon.stub();
        processor('test', fn);
        let result
        result = (await request(app)
            .post('/enqueue?client=1')
            .send({type: 'test'})
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200)).body
        await awaitEvent(`jobDone.${result.id}`)
        expect(fn.calledOnce).to.be.true;
    });
    it('should return a 500 if there is an exception', async function () {
        processor('test', () => {
            throw new Error('Deliberate');
        });
        await request(app)
            .post('/process')
            .send([{type: 'test'}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(500);
    });
    it('should be able to retrieve some documents and update them', async function () {
        processor('test', async (process, {documents}) => {
            expect(documents['nAXOXDJKCM:database/test']).to.exist;
            expect(documents['nAXOXDJKCM:database/test']).to.equal(documents[0]);
            documents[0].hello = 14;
        });
        await request(app)
            .post('/process')
            .send([{type: 'test', documents: ['nAXOXDJKCM:database/test']}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        let [[{data}]] = await query(`SELECT *
                                      FROM \`database\`.test
                                      WHERE _id = 'nAXOXDJKCM:database/test' `);
        expect(data.hello).to.eq(14);
    });
    it('should have an error if there was no processor', async function () {
        let {body} = await request(app)
            .post('/process')
            .send([{type: 'test', documents: ['nAXOXDJKCM:database/test']}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        expect(body.errors[0]).to.eq('Nothing to do');

    });
    it('should be able to retrieve some references', async function () {
        processor('test', async (process, {references}) => {
            expect(references['nAXOXDJKCM:database/test']).to.exist;
            expect(references['nAXOXDJKCM:database/test']).to.equal(references[0]);
        });
        await request(app)
            .post('/process')
            .send([{type: 'test', references: ['nAXOXDJKCM:database/test']}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
    });
    it('should be able to deny a process from running', async function () {
        function deny(event, params) {
            params.canRun = false;
        }

        let fn = sinon.stub();
        processor('test', fn);
        events.on('can-run.*', deny);
        await request(app)
            .post('/process')
            .send({type: 'test'})
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        events.off('can-run.*', deny);
        expect(fn.called).not.to.be.true;
    });
    it('should be able to cancel a processor', async function () {
        processor('test', async (process, {documents}) => {
            expect(documents['nAXOXDJKCM:database/test']).to.exist;
            expect(documents['nAXOXDJKCM:database/test']).to.equal(documents[0]);
            documents[0].hello = 14;
        });
        processor.clear('test');
        await request(app)
            .post('/process')
            .send([{type: 'test', documents: ['nAXOXDJKCM:database/test']}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        let [[{data}]] = await query(`SELECT *
                                      FROM \`database\`.test
                                      WHERE _id = 'nAXOXDJKCM:database/test' `);
        expect(data.hello).to.eq(3);
    });
    it('should be able to extend a lock during processing', async function () {
        processor('test', async (process, {documents, extend}) => {
            expect(documents['nAXOXDJKCM:database/test']).to.exist;
            expect(documents['nAXOXDJKCM:database/test']).to.equal(documents[0]);
            documents[0].hello = 14;
            extend(40000);
        });
        await request(app)
            .post('/process')
            .send([{type: 'test', documents: ['nAXOXDJKCM:database/test']}])
            .set('Content-Type', 'application/json')
            .set('Accept', 'application/json')
            .expect(200);
        let [[{data}]] = await query(`SELECT *
                                      FROM \`database\`.test
                                      WHERE _id = 'nAXOXDJKCM:database/test' `);
        expect(data.hello).to.eq(14);
    });
});
