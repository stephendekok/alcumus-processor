const SAVE_TIMEOUT = 1000 * 10;

const {create} = require('alcumus-app-datasource-mutations');
const {Database: {query, escapeId, escape, where, whereAsync, order, projectAndIndex}, dataSourceEvents} = require('alcumus-data-source-server');
const update = require('./behaviour-object-updater');
const isObject = require('lodash/isObject');
const {generate} = require('shortid');
const {initialize} = require('alcumus-behaviours');

async function createTableIfNecessary(database, source) {
    await query(`
          CREATE DATABASE IF NOT EXISTS ${escapeId(database)}
        `)
    return await query(`
      CREATE TABLE IF NOT EXISTS ${escapeId(database)}.${escapeId(source)} (
            row_id INT NOT NULL AUTO_INCREMENT,
            _id VARCHAR(100) NOT NULL,
            data JSON,
            created TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
            modified TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            state VARCHAR(255) GENERATED ALWAYS AS (
               json_unquote( json_extract( data, '$._behaviours.state' ) )
            ),
            deleted int GENERATED ALWAYS AS (
               json_unquote( json_extract( data, '$._deleted' ) )
            ),
            PRIMARY KEY (row_id),
            UNIQUE KEY (_id),
            KEY (state),
            KEY (deleted)
      )
    `)
}
function databaseFrom(key, defaultValue) {
    let type = key.split(':').pop().trim();
    let parts = type.split('/');
    return parts.length > 1 ? parts : [defaultValue, parts[0]];
}

function processOptions(options) {
    let orderStmt = ''
    if (options && isObject(options) && options.orderStmt && isObject(options.orderStmt)) {
        orderStmt = order(options.orderStmt)
    } else if (options && isObject(options) && options.orderStmt) {
        orderStmt = options.orderStmt
    }

    let limitStmt = ''
    if (options && isObject(options) && options.take) {
        if (options.skip) {
            limitStmt = ` LIMIT ${options.skip}, ${options.take}`
        } else {
            limitStmt = ` LIMIT ${options.take}`
        }
    }

    return {orderStmt, limitStmt}
}

dataSourceEvents.on('committed.*', function (...params) {
    console.info('Did receive committed event', this.event, params);
});

module.exports = {
    query, escapeId, escape, where, whereAsync, databaseFrom, dataSourceEvents, projectAndIndex,
    count: async function (database, table, whereStmt) {
        if (!database) throw new Error('Must specify a database');
        if (!table) throw new Error('Must specify a table');
        if (whereStmt && isObject(whereStmt)) {
            whereStmt = await whereAsync(whereStmt, undefined, table, database);
        }
        try {
            // language=MySQL
            let [rows] = await query(`SELECT count('_id') as recordcount
            FROM ${escapeId(database + '.' + table)} ${whereStmt || ''}  `);
            return rows[0].recordcount;
        } catch (e) {
            console.log(e)
            return e;
        }
    },
    list: async function (database, table, whereStmt, options) {

        if (!database) throw new Error('Must specify a database');
        if (!table) throw new Error('Must specify a table');
        if (whereStmt && isObject(whereStmt)) {
            whereStmt = await whereAsync(whereStmt, undefined, table, database);
        }

        let {orderStmt, limitStmt} = processOptions(options)

        try {
            if (process.env.DEBUG_SQL) {
                console.info("LIST", `SELECT _id
                FROM ${escapeId(database + '.' + table)} ${whereStmt || ''} ${orderStmt || ''} 
                ${limitStmt}`)
            }
            // language=MySQL
            let [rows] = await query(`SELECT _id
            FROM ${escapeId(database + '.' + table)} ${whereStmt || ''} ${orderStmt || ''} 
            ${limitStmt}`);
            return rows;
        } catch (e) {
            return [];
        }
    },
    listWithData: async function (database, table, whereStmt, options) {
        if (!database) throw new Error('Must specify a database');
        if (!table) throw new Error('Must specify a table');
        if (whereStmt && isObject(whereStmt)) {
            whereStmt = await whereAsync(whereStmt);
        }

        let {orderStmt, limitStmt} = processOptions(options)

        try {
            // language=MySQL
            if (process.env.DEBUG_SQL) {
                console.info("LISTWITHDATA", `SELECT _id, data
                    FROM ${escapeId(database + '.' + table)} ${whereStmt || ''}  ${orderStmt || ''} 
                    ${limitStmt}
                    
                    `)
            }

            let [rows] = await query(`SELECT _id, data
            FROM ${escapeId(database + '.' + table)} ${whereStmt || ''}  ${orderStmt || ''} 
            ${limitStmt}
            
            `);

            return rows;
        } catch (e) {
            return [];
        }
    },
    assign: async function (id, data) {
        let current = (await module.exports.get(id));
        if (current) {
            Object.assign(current, data);
            await module.exports.set(id);
        } else {
            current = await create(data)
        }
        return current;
    },
    get: async function (id) {
        if (!id) throw new Error('Must be supplied an id');
        const [database, table] = databaseFrom(id);
        if (!table) throw new Error('id must include a :<database>/<table>');
        if (!database) throw new Error('id must include a :<database>/<table>');
        // language=MySQL
        if (process.env.DEBUG_SQL) {
            console.info("GET", `SELECT data FROM ${escapeId(database + '.' + table)} WHERE _id=${escape(id)}`)
        }
        let rows;
        try {
            [rows] = await query(`SELECT data FROM ${escapeId(database + '.' + table)} WHERE _id=${escape(id)}`);
        } catch (e) {
            rows = []
        }
        if (!rows.length) return null
        let [{data}] = rows
        if (data._behaviours) {
            initialize(data);
        }
        data._original = JSON.parse(JSON.stringify(data));
        return data;
    },
    set: async function (data, id, timeout) {
        id = id || data._id;
        timeout = timeout || SAVE_TIMEOUT

        if (!id) throw new Error('Must supply an id');
        if (!data._original) throw new Error('Must pass back the object retrieved from \'get\'');
        let original = data._original;
        delete data._original;
        let lastMutation = update(original, data);
        if (lastMutation) {
            await new Promise((resolve, reject) => {
                let uq = lastMutation.uq;
                let savedAtAll = false
                const checkForSave = (committed) => {
                    savedAtAll = true
                    if (committed[uq]) {
                        resolve();
                        dataSourceEvents.off(`committed.${id}`, checkForSave);
                    }
                };
                setTimeout(() => {
                    dataSourceEvents.off(`committed.${id}`, checkForSave);
                    if (savedAtAll) {
                        resolve();
                    } else {
                        reject(new Error('Save timed out'))
                    }
                }, timeout);
                dataSourceEvents.on(`committed.${id}`, checkForSave);
            });
        }
        data._original = JSON.parse(JSON.stringify(data));
        return data;
    },
    put: async function ({_original, ...data}) {
        if (!isObject(data)) throw new Error('You must supply a data object');
        let id = data._id;
        if (!id) throw new Error('You must supply an id');
        let type = id.split(':')[1];
        if (!type) throw new Error('You must supply a type');
        if (type.split('/').length !== 2 || type.split('/').some(t => t.length === 0)) throw new Error('Type must be <database>/<table>');
        let [database, table] = type.split('/')
        await createTableIfNecessary(database, table)
        let dataEscaped = escape(JSON.stringify(data))
        if (process.env.DEBUG_SQL) {
            console.info("PUT", `INSERT INTO ${escapeId(database)}.${escapeId(table)} (_id, data) VALUES(${escape(data._id)}, ${dataEscaped}) ON DUPLICATE KEY UPDATE data=${dataEscaped}`)
        }
        for(let count = 0; count < 5; count++) {
            try {
                await query(`INSERT INTO ${escapeId(database)}.${escapeId(table)} (_id, data) VALUES(${escape(data._id)}, ${dataEscaped}) ON DUPLICATE KEY UPDATE data=${dataEscaped}`)
                return data
            } catch(err) {

            }

        }
        return data

    },
    create: async function (data, timeout) {
        if (!isObject(data)) throw new Error('You must supply a data object');
        let id = data._id;
        timeout = timeout || SAVE_TIMEOUT
        if (!id) throw new Error('You must supply an id');
        let type = id.split(':')[1];
        if (data._original) throw new Error('Object has already been created');
        if (!type) throw new Error('You must supply a type');
        if (type.split('/').length !== 2 || type.split('/').some(t => t.length === 0)) throw new Error('Type must be <database>/<table>');
        let result = create(type, data);
        let mutationUniqueId = create._result.uq;
        await new Promise((resolve, reject) => {
            const checkForSave = (committed) => {
                console.info('Will check (create)', committed, mutationUniqueId);
                if (committed[mutationUniqueId]) {
                    resolve();
                    dataSourceEvents.off(`committed.${id}`, checkForSave);
                }
            };
            setTimeout(() => reject(new Error('Save timed out')), timeout);
            console.info('Will commence checking create', `committed.${id}`);
            dataSourceEvents.on(`committed.${id}`, checkForSave);
        });
        result._original = JSON.parse(JSON.stringify(result));
        return result;
    },
    createLikeness(item) {
        let result = JSON.parse(JSON.stringify(item));
        delete result._original;
        const [database, table] = databaseFrom(result._id);
        result._id = `${generate()}:${database}/${table}`;
        return result;
    },
    initialize(database, type, target = {}) {
        target._id = `${generate()}:${database}/${type}`;
        return target;
    },
    initializeWithBehaviours(database, type, target = {}) {
        let result = module.exports.initialize(database, type, target);
        initialize(result);
        return result;

    },
    async access(defaultObject) {
        if (!defaultObject._id) throw new Error("Must have an id");
        let current = null
        try {
            current = await module.exports.get(defaultObject._id);
        } catch(e) {
            if (current) return current;
        }
        return current || await module.exports.create(defaultObject);
    }
};
