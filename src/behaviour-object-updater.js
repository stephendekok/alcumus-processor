const {set} = require('alcumus-app-datasource-mutations');
const isEqual = require('lodash/isEqual');
const isObject = require('lodash/isObject');
const setAll = Symbol('all')

function checkProperties(original, updated) {
    if (!original) return 1000;
    let count = 0;
    Object.keys(original).every(key => {
        if (!isEqual(original[key], updated[key])) count++;
        if (updated[key] === undefined) count += 1000;
    });
    return count;
}

const allowedKeys = ['_deleted', '_behaviours']
const disallowedKeys = ['behaviours']

function recurseSet(original, updated, base, accumulated = []) {
    let lastMutation = null;
    Object.keys(updated).filter(key => allowedKeys.includes(key) || !key.startsWith('_') || key === 'behaviours').forEach(property => {
        const updatedValue = updated[property];
        const originalValue = original[property];

        if (!isEqual(originalValue, updatedValue)) {
            if (originalValue && isObject(updatedValue) && (!Array.isArray(updatedValue) || updatedValue.length === (originalValue || []).length) && checkProperties(originalValue, updatedValue) < 4) {
                lastMutation= recurseSet(originalValue, updatedValue, base, [...accumulated, property]);
            }
            else {
                lastMutation = set([...accumulated, property].join('.'), updatedValue, base);
            }
        }
    });
    return lastMutation;
}

module.exports = function (original, updated) {
    let originalBehaviours = original._behaviours || {instances: {}};
    let updatedBehaviours = updated._behaviours || {instances: {}};
    let lastMutation = recurseSet(original, updated, original);
    if (originalBehaviours._state !== updatedBehaviours._state) {
        lastMutation = set('behaviours.state', updatedBehaviours._state, original);
    }
    return lastMutation;
};
