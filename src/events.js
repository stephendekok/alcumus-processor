const Events = require('alcumus-local-events/hooked-events');
const events = new Events({wildcard: true, maxListeners: 0});
module.exports = events;
