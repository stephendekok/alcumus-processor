# Alcumus Processor
This is a processor developed by alcumus

### Installation

```sh
npm i @alcumus/alcumus-processor
```

### Usage examples

```js
const { processor } = require('alcumus-processor');

processor('todo.add', async function ({payload}, {documents: [target]}) {
    target.todo = target.todo || [];
    target.todo.push(payload);
});
```